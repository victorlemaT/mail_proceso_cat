# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import datetime
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

def send_email(to_email, subjects, contents, api_key, from_emails, mime_type):
    try:
        message = Mail(
            from_email=from_emails,
            to_emails=to_email,
            subject=subjects,
            html_content=contents

        )
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "), "INFO ", "Enviando correo mendiante Sendgrid a ", file=open("output.txt", "a"))
        sg = SendGridAPIClient(api_key)
        response = sg.send(message)
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "), "INFO ", "Email enviado correctamente ",
               file=open("output.txt", "a"))
    except Exception as ex:
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "), "ERROR", "Error Exepc Sendgrid: ",
             ex, file=open("output.txt", "a"))
