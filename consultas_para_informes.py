
#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import configparser
import pymysql
import datetime
import sendgridendesa as sgend
import time
parser = argparse.ArgumentParser(description='Descripción de parámetros de entrada',
                                 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('filepath', help='Configuration file', type=str)
args = vars(parser.parse_args())
filename = args.get('filepath')

# Lectura de Fichero para datos
configParser = configparser.RawConfigParser()
configParser.read(filename)
endesagcc_host = configParser.get('endesagcc', 'local')
endesagcc_user = configParser.get('endesagcc', 'usuario')
endesagcc_per = configParser.get('endesagcc', 'permiso')
cendesagcc_bd = configParser.get('endesagcc', 'base_datos')
entorno_cat_list = configParser.get('entorno_cat', 'datos')
parametros = configParser.get('entorno_cat', 'parametros')
entorno_cat_arr = entorno_cat_list.split(";")
#DATOS DE CORREO
from_email = configParser.get('sendgrid', 'FROM_EMAIL')
sendgrid_key = configParser.get('sendgrid', 'APIKEY')
TO_EMAIL = configParser.get('notification-emails', 'to_email')

# funciones
#id para las consultas
def id_consultas():
    for entorno in entorno_cat_arr:
        f = ""
        f = f + " " + str(entorno)
    return (f)

#funciona dias
def last_date():
    lastDay = []
    today = datetime.date.today()
    #elemento dia
    lastDay.append(today - datetime.timedelta(days=1))
    lastDay.append(today - datetime.timedelta(days=2))
    lastDay.append(today - datetime.timedelta(days=3))
    lastDay.append(today - datetime.timedelta(days=4))
    lastDay.append(today - datetime.timedelta(days=5))
    return lastDay

#funcion cadena
def cambio_elementos(cadena):
    cambio_datos = cadena.replace(" '',", ",")
    cadena_lista = cambio_datos.lstrip(",")
    arrar = cadena_lista.split(",")
    return (arrar)

#funcion consulta
def consulta_f():
    cadena = ""
    p= ""
    for entorno in entorno_cat_arr:
        cadena = ""
        query = "(select 'ID',opclass from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and opclass = "+entorno+" )" \
           "union (select 'TOTAL', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and opclass = "+entorno+" )" \
           "union (select 'PENDIENTES', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and opclass = "+entorno+" and state=0)" \
           "union (select 'ACCESO', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and opclass = "+entorno+" and state=1)" \
           "union (select 'FIRMADAS', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and opclass = "+entorno+" and state=2)" \
           "union (select 'DIGITALIZADAS', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and opclass = "+entorno+" and state=50)" \
           "union (select 'CANCELADAS', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and opclass = "+entorno+" and state=99)"
        cnx = pymysql.connect(endesagcc_host, endesagcc_user, endesagcc_per, cendesagcc_bd)
        cursor = cnx.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        for pt in data:
            elemento = pt[0]
            elemento1 = pt[1]
            cadena = cadena + " "+str(elemento)+":"+str(elemento1)
        p = p +" "+cadena+","
    return (p)

def fila_total():
    cadena = ""
    query = "(select  'CANCELADAS', count(creationdate)  from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773  ) and state= 0 )union(select  'DIGITALIZADAS', count(creationdate)  from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773  ) and state=1 )union(select  'FIRMADAS', count(creationdate)  from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773  ) and state=2 )union(select  'ACCESO', count(creationdate)  from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773  ) and state=50 )union (select  'PENDIENTE', count(creationdate)  from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773  ) and state=99 )"
    cnx = pymysql.connect(endesagcc_host, endesagcc_user, endesagcc_per, cendesagcc_bd)
    cursor = cnx.cursor()
    cursor.execute(query)
    data = cursor.fetchall()
    fila = ""
    for pt in data:
        #print(pt[1])
        fila = fila + "<td style=' border: 2px solid #a7b6d6; text-align: center; padding: 8px'>" + str(pt[1])+"</td>"
        #print(fila)

    return (fila)

#funcion totales
def consulta_totales():
    cadena = ""
    p= ""
    for entorno in entorno_cat_arr:
        cadena = ""
        query2 = "(select 'TOTAL', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' ) " \
           "union (select 'PENDIENTES', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773) and state = 0 ) " \
           "union (select 'ACCESO', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773) and state = 1 ) " \
           "union (select 'FIRMADAS', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773) and state = 2 ) " \
           "union (select 'DIGITALIZADAS', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773) and state = 50 ) " \
           "union (select 'CANCELADAS', count(creationdate) from docsmanager_cat.operation o where creationdate LIKE '%"+str(i)+"%' and (opclass = 7770 or opclass = 7771 or opclass = 7774 or opclass = 7802 or opclass = 7773) and state = 99 ) "
        cnx = pymysql.connect(endesagcc_host, endesagcc_user, endesagcc_per, cendesagcc_bd)
        cursor = cnx.cursor()
        cursor.execute(query2)
        data = cursor.fetchall()
        for pt in data:
            elemento = pt[0]
            elemento1 = pt[1]
            cadena = cadena + " "+str(elemento)+":"+str(elemento1)
        p = p +" "+cadena+","
    return (p)

days = last_date()
numdays = ""
wr = ""


def run_query(query):
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "), "INFO ", " CONSULTA REALIZADA",
          file=open("output.txt", "a"))

    # Connect
    cnx = pymysql.connect(endesagcc_host, endesagcc_user, endesagcc_per, cendesagcc_bd)
    cursor = cnx.cursor()
    # Execute SQL select statement
    cursor.execute(query)
    # Save all datas inside array data
    data = []
    data.append(cursor.fetchone())
    while data[-1] is not None:
        data.append(cursor.fetchone())
    # Close the connection
    cursor.close()
    cnx.close()
    # return all elements except the last, because it is None
    return data[:-1]


query = ("SELECT * FROM docsmanager_cat.operation WHERE creationDate LIKE '%{day_date}%'; ")
query2 = ("SELECT * FROM docsmanager_cat.operation WHERE creationDate LIKE '%{day_date}%' AND state=50; ")

cuerpo_correo1 = ""
result_CREADAS_AYER = []
result_DIGITALIZADAS_AYER = []
for i in range(len(days)):
    query_CREADAS_AYER = query.format(day_date=days[i].strftime('%Y-%m-%d'))
    result_CREADAS_AYER = run_query(query_CREADAS_AYER)
    query_DIGITALIZADAS_AYER = query2.format(day_date=days[i].strftime('%Y-%m-%d'))
    result_DIGITALIZADAS_AYER = run_query(query_DIGITALIZADAS_AYER)
    operaciones_digitalizadas = len(result_DIGITALIZADAS_AYER)
    operaciones_creadas = len(result_CREADAS_AYER)
    # operaciones_finalizadas = (operaciones_digitalizadas * 100) / operaciones_creadas
    # cuerpo_correo1 = cuerpo_correo1 + ('''<p>El día {lastDay} se crearon {operaciones_creadas} operaciones, de las cuales finalizaron exitosamente el {operaciones_finalizadas} % </p>''').format(lastDay=days[i].strftime('%Y-%m-%d'),                                                                                                                                                                                       operaciones_creadas=len(result_CREADAS_AYER), operaciones_finalizadas= round((operaciones_digitalizadas * 100) / operaciones_creadas))
    #cuerpo_correo1 = cuerpo_correo1 + ('''<p>El día {lastDay} se crearon {operaciones_creadas} operaciones</p>''').format(lastDay=days[i].strftime('%Y-%m-%d'), operaciones_creadas=len(result_CREADAS_AYER))

    if (operaciones_creadas != 0):
        operaciones_finalizadas = (operaciones_digitalizadas * 100) / operaciones_creadas
        cuerpo_correo1 = cuerpo_correo1 + (
            '''<p>El día {lastDay} se crearon {operaciones_creadas} operaciones, de las cuales finalizaron exitosamente el {operaciones_finalizadas} % </p>''').format(
            lastDay=days[i].strftime('%Y-%m-%d'), operaciones_creadas=len(result_CREADAS_AYER),
            operaciones_finalizadas=round((operaciones_digitalizadas * 100) / operaciones_creadas))
    else:
        cuerpo_correo1 = cuerpo_correo1 + (
            '''<p>El día {lastDay} se crearon {operaciones_creadas} operaciones</p>''').format(
            lastDay=days[i].strftime('%Y-%m-%d'), operaciones_creadas=len(result_CREADAS_AYER))
wr1 = ""
wr1 = wr1 + " " + cuerpo_correo1

#creacion de las tablas por dias
for i in days:
    numdays = numdays + i.strftime('%Y-%m-%d') + " , "
    cadena_p = consulta_f()
    #solo una fila
    arrar_01 = cambio_elementos(cadena_p)
    ff = ""
    for p in arrar_01:
        elemento = p

        ppp = elemento.split(" ")
        f1 = ""
        for gt in ppp:
            elemento01 = gt
            pppp = elemento01.split(":")
            for fg in pppp[1:2]:
                f1 = f1+"<td style=' border: 2px solid #a7b6d6; text-align: center; padding: 8px'>"+fg+"</td>"
                #print(f1)
        ff = ff+"<tr>"+f1+"</tr>"

    cuerpo_correo = ('''<br> 
                                <table style="width:80%; font-size: 13px; line-height: 20px; ">
                                    <tr><td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0 ;" colspan="7">OPERACIONES DE ENDESA DEL DIA   : <b>{fechadia}</b></td></tr>
                                    <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> ID</td>     
                                    <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> TOTAL</td> 
                                    <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; ">PENDIENTE</td> 
                                    <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> ACCESO</td> 
                                    <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> FIRMADAS</td> 
                                    <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> DIGITALIZADAS</td> 
                                    <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> CANCELADAS</td>        
                                    <tr><td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #FF0C00; " >Pro Controlado </td><td style=' border: 2px solid #a7b6d6; text-align: center; padding: 8px'>total</td>{total}</tr>                            
                                    {fin}
                                </table>
                    ''').format(fechadia=i, fin=ff, total=fila_total())
    wr = wr + " " + cuerpo_correo
html_final = ('''<html> 
                        <head>
                                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        </head>
                            <body  width="80%" style="margin: 0; font-family: sans-serif;"  text= "#424242" > 
                            <p>Buenos días,
                            <p>Producción de EndesaCAT ha pasado satisfactoriamente los test esta mañana y este es el resumen de las operaciones de los últimos 5 dias :
                            {cuerpo}
                            </body> 
                </html> ''').format(cuerpo= wr1 + wr)
arrr_dia = numdays.split(",")

subject = "[CAT] Resumen Operaciones" + time.strftime('%Y-%m-%d')
#envio de correo por destinos
to_email_arr = TO_EMAIL.split(";")

print(html_final,file=open("outputr.txt", "a"))
for to_emailf in to_email_arr:
    #llamamos a la funcion correo
    sgend.send_email(to_emailf, subject, html_final, sendgrid_key, from_email, 'text/html')